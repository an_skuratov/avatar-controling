﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class LightSaberCollision : MonoBehaviour {

    public GameObject boxToBeat;
    public GameObject boxToBeatSystem;

    public static LightSaberCollision instance;

    private Quaternion[] littleBoxesDefaultRotation;
    private Vector3[] littleBoxesDefaultPosition;
    private Vector3 boxToBeatSystemDefPosition;
    private Vector3 boxToBeatDefPosition;

	// Use this for initialization
	void Start () {
        littleBoxesDefaultRotation = new Quaternion[64];
        littleBoxesDefaultPosition = new Vector3[64];

        boxToBeatDefPosition = boxToBeat.transform.position;
        boxToBeatSystemDefPosition = boxToBeatSystem.transform.position;

        for (int i = 0; i < boxToBeatSystem.transform.childCount; ++i)
        {
            littleBoxesDefaultPosition[i] = boxToBeatSystem.transform.GetChild(i).transform.position;
            littleBoxesDefaultRotation[i] = boxToBeatSystem.transform.GetChild(i).transform.rotation;
        }

        boxToBeatSystem.SetActive(false);
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.Equals(boxToBeat))
        {
            boxToBeatSystem.SetActive(true);
            boxToBeat.SetActive(false);
        }
    }

    public static void ResetObjectsStatements()
    {
        instance.boxToBeat.transform.position = instance.boxToBeatDefPosition;
        instance.boxToBeatSystem.transform.position = instance.boxToBeatSystemDefPosition;

        for (int i = 0; i < instance.boxToBeatSystem.transform.childCount; ++i)
        {
            instance.boxToBeatSystem.transform.GetChild(i).transform.position = instance.littleBoxesDefaultPosition[i];
            instance.boxToBeatSystem.transform.GetChild(i).transform.rotation = instance.littleBoxesDefaultRotation[i];
        }

        instance.boxToBeat.SetActive(true);
        instance.boxToBeatSystem.SetActive(false);
    }
}
