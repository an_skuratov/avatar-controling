﻿using UnityEngine;
using System.Collections;

public class BBModelMoving : MonoBehaviour {

    private float deltaX;
    private float speed;
    private float angleValue;

    private GameObject head;
    private GameObject body;
	// Use this for initialization
    void Start()
    {
        deltaX = 0;
        speed = 0.05f;
        angleValue = -30;
        
        head = GameObject.Find("Object004");
        body = GameObject.Find("Box001");
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Mathf.Abs(deltaX) > 30)
        {
            speed = -speed;
            gameObject.transform.Rotate(new Vector3(angleValue, 0, angleValue));
            angleValue = -angleValue;
        }

        if (speed > 0)
        {
            body.transform.Rotate(new Vector3(7, 0, 7));
        }
        else if (speed < 0)
        {
            body.transform.Rotate(new Vector3(-7, 0, -7));
        }

        gameObject.transform.position += new Vector3(speed, 0, speed);
        deltaX += speed;
    }
}
