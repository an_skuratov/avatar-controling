﻿using UnityEngine;
using System.Collections;

public class BBHeadRotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject == null)
            return;

        gameObject.transform.Rotate(new Vector3(0, 0, 1.2f));
	}
}
